# ECE 445 Lab Notebook


## 2.8.2024
### Objectives
- Write project proposal for SnapLog camera necklace

### Work Done
We started working on the project proposal for our SnapLog camera necklace. We outlined the main ideas and features we want to include, such as:

- Automatic time-lapse video creation from photos taken at regular intervals
- Wireless transmission of photos to user's device
- Long battery life and small form factor

I also did some initial research on potential components we could use, like microcontrollers with Bluetooth capabilities and low-power camera sensors.

<p align="center">
  <img src="pic/visual.jpg">
</p>

## 2.9.2024
### Objectives
- Finalize team contract

### Work Done
Our team met today to discuss and finalize the team contract. We outlined each member's roles and responsibilities, agreed on a schedule for team meetings, and set expectations for communication and conflict resolution.

We also discussed potential risks and challenges we might face during the project and brainstormed ways to mitigate them.

## 2.22.2024
### Objectives
- Write design document for SnapLog

### Work Done
We detailed the subsystem design, breaking down the project into different components like the power subsystem, sensor subsystem, transmission subsystem, and imaging subsystem.

I also researched and selected potential core components, like the ESP32 microcontroller and the OmniVision OV5640 camera sensor, based on our requirements.

Additionally, we included initial hardware design diagrams and a proposed prototyping process.

<p align="center">
  <img src="pic/block_diagram.jpg">
</p>

## 2.26.2024
### Objectives
- Order first revision of PCB

### Work Done
Today, we reviewed the design document and finalized the first revision of our PCB design. We decided to split the design into three separate bring-up boards for initial testing and debugging.

I placed the order for the PCB fabrication and components from our selected vendors. While waiting for the boards to arrive, I started working on the firmware and setting up the development environment.

## 3.12.2024
### Objectives
- Test and debug Rev. A boards

### Work Done
The Rev. A boards arrived today, and we began assembling and testing them. We encountered some issues, such as a disconnected ground plane, which prevented the boards from functioning correctly.

We spent the day debugging the boards, applying solder bridges, and making temporary fixes to test the individual components. This experience helped us identify areas for improvement in the next revision.

## 3.25.2024
### Objectives
- Order second revision of PCB (Rev. B)

### Work Done
After analyzing the issues from Rev. A, we made necessary changes to the design and prepared for the second revision of the PCB. We decided to combine the three bring-up boards into a single board to improve the form factor.

Additionally, we had to swap out some components, like the microcontroller and power management IC, to reduce costs while maintaining functionality.

I placed the order for the Rev. B PCBs and components today.

## 4.2.2024
### Objectives
- Test and debug Rev. B boards

### Work Done
The Rev. B boards arrived, and we began testing them. We were able to get the camera demonstration code provided by the manufacturer running on the hardware, which was a significant milestone.

However, we still encountered some minor issues that needed to be addressed in the next revision. We documented these issues and started working on the design for Rev. C.

<p align="center">
  <img src="pic/high_level_schematic.jpg">
</p>

## 4.10.2024
### Objectives
- Finalize Rev. C design
- Order Rev. C PCBs

### Work Done
Today, we finalized the design for the Rev. C PCBs, incorporating all the changes and fixes identified during the testing of previous revisions. We thoroughly reviewed the design to ensure it met all our requirements and addressed any potential issues.

Once the design was approved, I placed the order for the Rev. C PCBs and components.

## 4.16.2024
### Objectives
- Prepare for mock demo

### Work Done
With the Rev. C boards on their way, we started preparing for the mock demo. I worked on integrating the firmware with the wireless transmission protocol and setting up the GUI for configuring the camera parameters.

We also tested the timelapse video creation functionality using sample images to ensure a smooth end-to-end experience for the demo.

## 4.20.2024
### Objectives
- Test and debug Rev. C boards
- Verify design requirements

### Work Done
The Rev. C boards arrived today, and we immediately started testing and debugging them. To our relief, the boards performed as intended, and we were able to verify that all the design requirements were met.

We tested the automatic picture capture at configurable intervals, manual image capture, wireless transmission, and video compilation functionalities. Everything worked seamlessly, and we were ready for the final demo and presentation.

## 4.22.2024
### Objectives
- Final demo and presentation preparation

### Work Done
Today, we spent time rehearsing and polishing our final demo and presentation. We ensured that all the components were working correctly and that we had a smooth flow for demonstrating the SnapLog features.

We also prepared any necessary slides, videos, or other materials to support our presentation and showcase the project effectively.

<p align="center">
  <img src="pic/subsystem.jpg">
</p>

## 4.27.2024
### Objectives
- Finalize presentation materials
- Practice demo and presentation

### Work Done
With the final demo and presentation just around the corner, we dedicated today to finalizing all the presentation materials and ensuring a seamless flow for the demo.

We practiced our roles and transitions, making any necessary adjustments to the content or flow based on our rehearsals. We also double-checked that all the hardware and software components were functioning correctly for the demo.

<p align="center">
  <img src="pic/enclosure.jpg">
</p>

## 4.29.2024
### Objectives
- Final presentation and demo

### Work Done
Today was the day of our final presentation and demo for the SnapLog camera necklace project. We successfully showcased the automatic timelapse video creation, wireless transmission, long battery life, and user-friendly interface of our product.

The demo went smoothly, and we received positive feedback from the audience. We also discussed potential future improvements and enhancements we could explore for SnapLog.

Overall, it was a rewarding experience to see our hard work and dedication come to fruition in the form of a fully functional and innovative product.